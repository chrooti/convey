defmodule Convey.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    dispatch = [
      {:_, [
        {"/rt", Convey.Realtime.Plug, nil},
        {:_, Plug.Cowboy.Handler, {Convey.Router.Plug, []}}
      ]}
    ]

    # tood port from env
    children = [
      Plug.Cowboy.child_spec(scheme: :http, plug: nil, port: 4001, dispatch: dispatch),
      {Convey.Board, name: Convey.Board}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: Convey.Supervisor)
  end
end
