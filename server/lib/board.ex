defmodule Convey.Board do
  use GenServer

  # defp broadcast_state(%{clients: clients, text: text}) do
  #   for %{pid: pid, cursor: cursor} <- clients do
  #     GenServer.cast(pid, {:text, text})
  #   end
  # end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  @impl true
  def init(_state) do
    {:ok, %{
      clients: %{},
      global: %{
        content: "" # todo: keep it saved somewhere?
      },
    }}
  end

  @impl true
  def handle_call(:register, {pid, _}, state) do
    client = %{
      # track revision here
      id: :crypto.strong_rand_bytes(32) |> Base.url_encode64
    }

    state = put_in(state.clients[pid], client)

    {:reply, {:content, state.global, client, {0, 0}}, state}
  end

  @impl true
  def handle_call(:unregister, {pid, _}, state) do
    {_, state} = pop_in(state.clients[pid])

    {:reply, :ok, state}
  end

  @impl true
  def handle_cast({:insert, new_content, cursor_start, cursor_end}, state) do
    {content_start, content_end} = String.split_at(state.global.content, cursor_start)

    cursor_delta = cursor_end - cursor_start
    content_end = String.slice(content_end, cursor_delta..-1//1)

    content = content_start <> new_content <> content_end

    state = put_in(state.global.content, content)

    for {pid, client} <- state.clients do
      edit_range = {cursor_start, cursor_start + String.length(new_content)}
      GenServer.cast(pid, {:content, state.global, client, edit_range})
    end

    {:noreply, state}
  end


end
