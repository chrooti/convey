defmodule Convey.Router.Plug do
  use Plug.Router

  plug :match
  # insert json parser here for API? or delegate to other router
  plug :dispatch

  # forward "/api" to: api.ex?

  match "/" do
    serve_file(conn, "/app.html")
  end

  match _ do
    serve_file(conn, conn.request_path)
  end

  defp serve_file(conn, path) do
    path = if String.contains?(path, "."), do: path, else: "/app.html"

    case File.read("../client#{path}") do
      {:ok, content} ->
        mime = MIME.from_path(path)

        conn |>
        put_resp_content_type(mime) |>
        send_resp(200, content)

      {:error, _error} ->
        conn |>
        send_resp(404, "")
    end

  end

end
