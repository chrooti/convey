defmodule Convey.Realtime.Plug do
  @behaviour :cowboy_websocket

  defp api_set_content(global, client, {edit_start, edit_end}) do
    %{
      type: "setContent",
      content: global.content,
      clientId: client.id,
      editStart: edit_start,
      editEnd: edit_end,
    }
  end

  @impl :cowboy_websocket
  def init(req, state) do
    {:cowboy_websocket, req, state}
  end

  @impl :cowboy_websocket
  def websocket_init(state) do
    {:content, global, client, edit_range} = GenServer.call(Convey.Board, :register)

    json = [
      %{type: "init", clientId: client.id},
      api_set_content(global, client, edit_range)
    ] |> Jason.encode!

    {:reply, {:text, json}, state}
  end

  @impl :cowboy_websocket
  def websocket_handle({:text, message}, state) do
    msg = Jason.decode!(message)

    case Map.get(msg, "type") do
      "insert" ->
        %{
          "cursorStart" => cursor_start,
          "cursorEnd" => cursor_end,
          "content" => content
        } = msg;

        GenServer.cast(Convey.Board, {:insert, content, cursor_start, cursor_end})
      "ping" ->
        :noop
      _ ->
        IO.inspect msg
    end

    {:ok, state}
  end

  @impl :cowboy_websocket
  def websocket_info({:"$gen_cast", {:content, global, client, edit_range}}, state) do
    json = [
      api_set_content(global, client, edit_range)
    ] |> Jason.encode!
    {:reply, {:text, json}, state}
  end

  @impl :cowboy_websocket
  def websocket_info({:"$gen_cast", msg}, state) do
    IO.inspect msg
    {:ok, state}
  end

  @impl :cowboy_websocket
  def terminate(_reason, _req, _state) do
    GenServer.call(Convey.Board, :unregister)
    :ok
  end
end
